// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyChfVXlxiJkyA5zDRypV4EEwbmu2aOo1hs",
    authDomain: "loginionic-5e42b.firebaseapp.com",
    databaseURL: "https://loginionic-5e42b.firebaseio.com",
    projectId: "loginionic-5e42b",
    storageBucket: "",
    messagingSenderId: "930600723677",
    appId: "1:930600723677:web:fdab0b15260133977f6734"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
