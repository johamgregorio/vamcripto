import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { User } from '../shared/user.class';
import { LoadingController, ToastController, MenuController, AlertController } from '@ionic/angular';
import { async } from 'q';
import { Button } from 'protractor';
import * as firebase from 'firebase';
import { prepareSyntheticListenerName } from '@angular/compiler/src/render3/util';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  mensaje: String;
  open: boolean = true;
  user: User = new User();
  email: string ="";
  password: string ="";
  authUser;

  constructor(private router: Router, 
    private authSvc: AuthService, 
    private loadingCtrl: LoadingController, 
    private toastCtrl: ToastController, 
    public loadingController: LoadingController,
    public menuCtrl: MenuController,
    private alertCtrl: AlertController
    ) {}

  ngOnInit() {
    
  }
  
 async onLogin(){
  const user = await this.authSvc.onLogin(this.user);
  this.email = this.user.email;
  this.password = this.user.password;

  if (user){

    const loading = await this.loadingController.create({
      message: 'Cargando',
      duration: 3000
    });
    await loading.present();
    console.log('Usuario Logeado Correctamente');
    this.router.navigateByUrl('/home');

   

  }else if(this.user.email){
    const toast = await this.toastCtrl.create({
      message: "Correo/Contraseña Invalida",
      duration: 1000,
      position:"bottom",
  
     });
     toast.present();
   }else if (this.user.password){
    const toast = await this.toastCtrl.create({
      message: "Contraseña/Correo Invalido",
      duration: 1000,
      position:"bottom",
  
     });
     toast.present();
   }
  }
  
  olvido(email){
    firebase
    .auth()
    .sendPasswordResetEmail(email)
    .then(function() {
      alert("Correo Enviado")
    }).catch(function(error) {
    }); 
  }

}