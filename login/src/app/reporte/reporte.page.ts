import { Component, OnInit } from '@angular/core';
import validator from 'validator';
import { NavController, AlertController, ToastController } from '@ionic/angular';
import { FormBuilder, FormControl, Validators,FormGroup } from '@angular/forms';
import { NotaSQLlteService } from '../nota-sqllte.service';
import { async } from 'q';
import { ComprobanteService } from '../services/comprobante.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-reporte',
  templateUrl: './reporte.page.html',
  styleUrls: ['./reporte.page.scss'],
})

export class ReportePage implements OnInit {
  
  public form = {
    nombre: '',
    rif: '',
    telefono: '',
    numeroComprobante: ''
  };

  constructor(
    public navCtrl: NavController, 
    public router: Router,
    private route: ActivatedRoute,
    public alertCtrl: AlertController,
    public formBuilder: FormBuilder,
    public NotaSQL: NotaSQLlteService,
    public comprobanteService: ComprobanteService,
    public toastCtrl: ToastController) {}

  ngOnInit(): void {}

  reportar() {
    let form = this.form;
    this.comprobanteService.create({
      razonSocial: form.nombre,
      numero: form.numeroComprobante,
      status: 1
    });
    this.router.navigate(['/home']);
  }
}