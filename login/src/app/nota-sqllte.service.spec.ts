import { TestBed } from '@angular/core/testing';

import { NotaSQLlteService } from './nota-sqllte.service';

describe('NotaSQLlteService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NotaSQLlteService = TestBed.get(NotaSQLlteService);
    expect(service).toBeTruthy();
  });
});
