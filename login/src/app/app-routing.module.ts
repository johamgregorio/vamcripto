import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from  './guards/auth.guard';
import { ReactiveFormsModule } from '@angular/forms';


const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
  { path: 'register', loadChildren: './register/register.module#RegisterPageModule' },
  { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
  { path: 'admin', loadChildren: './admin/admin.module#AdminPageModule'},
  { path: 'catalogos', loadChildren: './catalogos/catalogos.module#CatalogosPageModule'},
  { path: 'reporte', loadChildren: './reporte/reporte.module#ReportePageModule'},
  { path: 'pago', loadChildren: './pago/pago.module#PagoPageModule'},
  { path: 'pago-mercantil', loadChildren: './pago/pago-mercantil/pago-mercantil.module#PagoMercantilPageModule' },
  { path: 'pago-venezuela', loadChildren: './pago/pago-venezuela/pago-venezuela.module#PagoVenezuelaPageModule' },  { path: 'consulta-comprobante', loadChildren: './home/consulta-comprobante/consulta-comprobante.module#ConsultaComprobantePageModule' },
  { path: 'criptomoneda', loadChildren: './pago/criptomoneda/criptomoneda.module#CriptomonedaPageModule' },


];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
