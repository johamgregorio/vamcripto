import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { User } from '../shared/user.class';

@Injectable({providedIn: 'root'})
export class CatalogoService {

    constructor() {}

    all() {
        return [{
            id: 1,
            name: 'Harina Pan',
            descripcion: 'Harina pan, producto proveniente de empresas Polar.',
            empresa: 'Empresas Polar',
            precioDolares: '25',
            precioBtc: '0,0029',
            img: 'assets/harinapan.jpg'
        }, {
            id: 2,
            name: 'Arroz Primor',
            descripcion: 'Arroz Primor, producto proveniente de empresas Polar.',
            empresa: 'Empresas Polar',
            precioDolares: '35',
            precioBtc: '0,0040',
            img: 'assets/arrozprimor.jpg'
        }, {
            id: 3,
            name: 'Azucar Montalban',
            descripcion: ' Azucar Montalban, producto proveniente de empresas Polar',
            empresa: 'Empresas Polar',
            precioDolares: '35',
            precioBtc: '0,0040',
            img: 'assets/azucarmontalban.jpg'
        }, {
            id: 4,
            name: 'Harina de Trigo Blancaflor',
            descripcion: 'Harina Blancaflor, producto proveniente MOLINOS RIO DE LA PLATA S.A.',
            empresa: 'MOLINOS RIO DE LA PLATA S.A.',
            precioDolares: '25',
            precioBtc: '0,0029',
            img: 'assets/blancaflor.jpg'
        }, {
            id: 5,
            name: 'Aceite Mazeite',
            descripcion: 'Aceite Mazeite, producto proveniente MOLINOS RIO DE LA PLATA S.A.',
            empresa: 'MOLINOS RIO DE LA PLATA S.A.',
            precioDolares: '35',
            precioBtc: '0,0040',
            img: 'assets/mazeite.jpg'
        }, {
            id: 6,
            name: 'Pasta Ronco',
            descripcion: 'Pasta Corta y Larga marca Ronco, producto proveniente de empresas Polar.',
            empresa: 'Empresas Polar',
            precioDolares: '35',
            precioBtc: '0,0040',
            img: 'assets/ronco.jpg'
        }]
    }


    find(id) {
        let x = this.all().filter(x=>x.id == id);
        if(!x.length) alert("producto invalido");
        return x[0];
    }

}