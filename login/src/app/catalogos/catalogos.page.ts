import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { CatalogoService } from './catalogo.service';

@Component({
  selector: 'app-catalogos',
  templateUrl: './catalogos.page.html',
  styleUrls: ['./catalogos.page.scss'],
})
export class CatalogosPage implements OnInit {

  public productos;

  constructor(
    private catalogoService: CatalogoService,
    public router: Router, 
    private navCtrl:NavController) { }

  ngOnInit() {
    this.productos = this.catalogoService.all();
  }
  
  pagar(producto){
    this.router.navigate(['/pago', producto.id]);
  }
}
