import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ConsultaComprobantePage } from './consulta-comprobante.page';

const routes: Routes = [
  {
    path: '',
    component: ConsultaComprobantePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ConsultaComprobantePage]
})
export class ConsultaComprobantePageModule {}
