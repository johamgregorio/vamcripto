import { Component, OnInit } from '@angular/core';
import { ComprobanteService } from 'src/app/services/comprobante.service';
import { CatalogoService } from 'src/app/catalogos/catalogo.service';

@Component({
  selector: 'app-consulta-comprobante',
  templateUrl: './consulta-comprobante.page.html',
  styleUrls: ['./consulta-comprobante.page.scss'],
})
export class ConsultaComprobantePage implements OnInit {

  public comprobantes;
  constructor(
    private catalogoService: CatalogoService,
    private comprobanteService: ComprobanteService) { }

  ngOnInit() { 
    this.comprobantes = this.comprobanteService.all();
  }

}
