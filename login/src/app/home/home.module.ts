import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { HomePage } from './home.page';

const routes: Routes = [
  {
    path: 'home',
    component: HomePage,
    children: [
      { 
        path: 'login', 
        loadChildren: '../login/login.module#LoginPageModule' 
      },
      { 
        path: 'home/admin', 
        loadChildren: './admin/admin.module#AdminPageModule'
      },
      { 
          path: 'home/catalogos', 
          loadChildren: './catalogos/catalogos.module#CatalogosPageModule'
      },
      {
        path: 'pago', 
        loadChildren: './pago/pago.module#PagoPageModule'
      },
      {
        path: 'reporte', 
        loadChildren: './reporte/reporte.module#ReportePageModule'
      },
      {
        path: 'consulta-comprobante',
        loadChildren: './consulta-comprobante/consulta-comprobante.module#ConsultaComprobantePageModule' 
      }
    ]
  },
  {
    path: '',
    redirectTo: '/home/admin'
  },
  {
    path: '',
    redirectTo: '/home/catalogos'
  },
  {
    path: '/catalogos',
    redirectTo: 'home/pago'
  },
  {
    path: '/reporte',
    redirectTo: 'home/reporte'
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: '',
        component: HomePage
      }
    ])
  ],
  declarations: [HomePage]
})
export class HomePageModule {}
