import { Component, OnInit } from '@angular/core';
import { Router, RouterEvent } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { AuthService } from '../services/auth.service';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { NotaSQLlteService} from '../nota-sqllte.service'

interface datos{
  nombreJurida : String
  rif : String
  productoId : String
  comprobanteId : String 

}


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  visible = true;

  pages = [
    {
      title: 'Consulta de Productos',
      url: '/consulta-comprobante'
    },
    {
      title: 'Catalogo de Producto',
      url: '/catalogos'
    },
    {
      title: 'Pago de Productos',
      url: '/pago'
    },
    {
      title: 'Reporte de Pago',
      url: '/reporte'
    },
    {
      title:'Mantenimiento Perfil',
      url: '/admin'
      },
    {
      title:'Salir',
      url: '/login'
    }
  ];

  selectedPath = '';
  validations_form: any;
  formBuilder: any;
  public consulta: any = [];

  constructor(
    private authSvc: AuthService, 
    private router: Router,
    private afAuth: AngularFireAuth,
    public notaSql : NotaSQLlteService
    ){
      setTimeout(() =>{
        this.visible = false;
      }, 3000);

      this.router.events.subscribe((event: RouterEvent) => {
        this.selectedPath = event.url;
      });
    }

    onLogout(){
      console.log("Hasta Pronto");
      this.afAuth.auth.signOut();
      this.router.navigateByUrl('/login');
    }
    validacion(){
    this.validations_form = this.formBuilder.group({
      name: new FormControl('', Validators.required),
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ]))
    });
  }

  ngOnInit(){
    this.notaSql.getDatosdeConsulta().subscribe( test => {
      test.map( tes => {
        console.log(tes.payload.doc.data())
        const info : datos = tes.payload.doc.data() as datos;
        info.productoId = tes.payload.doc.id;
        
        this.consulta.push(info);


      })
    })
  }

  comprobante() {
    console.log(3333);
    //this.router.navigate(['/consulta-comprobante']);
  }

}
