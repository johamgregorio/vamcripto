import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { User } from '../shared/user.class';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public authUser;
  public isLooged: any = false;
  constructor(public afAuth: AngularFireAuth) { 
    afAuth.authState.subscribe (user  => (this.isLooged = user));
  }

  // login
  async onLogin (user: User) {
    try {
      this.authUser = await this.afAuth.auth.signInWithEmailAndPassword(user.email, user.password); 
      this.saveAuthUser(this.authUser);      
      return this.authUser;
    } catch (error) {
      console.log('error en la validacion de usuario');
    }
  }

  // Registro

  async onRegister(user: User) {
    try {
      return await this.afAuth.auth.createUserWithEmailAndPassword(
        user.email,
        user.password
      )
    }
    catch{
      console.log('error registrando al usuario');
    }
  }

  updateEmail(email, nuevoEmail, password) {
    let user: User = {
      email: email,
      password: password
    };
    
    return this.afAuth.auth.signInWithEmailAndPassword(user.email, user.password)
    .then((userCredential)=>{
      userCredential.user.updateEmail(nuevoEmail);
    });
  }

  saveAuthUser(authUser) {
    let storage = window.localStorage;
    storage.setItem('userAuth', JSON.stringify( {
      id: authUser.user.uid
    }));
  }

  getAuthUser() {
    let storage = window.localStorage;
    let user = storage.getItem("userAuth");
    return JSON.parse(user);
  }
}
