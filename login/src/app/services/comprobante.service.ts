import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { User } from '../shared/user.class';
import { AngularFirestore } from '@angular/fire/firestore';
import { AuthService } from './auth.service';
import * as _ from 'lodash';

@Injectable({providedIn: 'root'})
export class ComprobanteService {

    private comprobantesKey = "TABLE_COMPROBANTE";
    
    constructor(private authService: AuthService) {
        this.set();
    }

    get() {
        let storage = window.localStorage;
        let table = storage.getItem(this.comprobantesKey);
        if(!table) return [];
        return JSON.parse(table);
    }

    set() {
        let storage = window.localStorage;
        if(!this.get()) storage.setItem(this.comprobantesKey, JSON.stringify([]));
    }

    create(comprobante) {
        let storage = window.localStorage;
        let table = this.get();
        table.push({
            razonSocial: comprobante.razonSocial,
            userId: this.authService.getAuthUser().id,
            numero: comprobante.numero,
            status: comprobante.status
        });
        storage.setItem(this.comprobantesKey, JSON.stringify(table));
    }

    findById(id) {
        let table = this.get();
        let elX = table.filter(x=>x.id == id);
        if(!elX.length) return null;
        return elX[0];
    }

    findByUser(userId) {
        let table = this.get();
        let elX = table.filter(x=>x.userId == userId);
        if(!elX.length) return null;
        return elX[0];
    }

    all() {
        let table = this.get();
        return table;
    }
}