import { Component, OnInit } from '@angular/core';
import { CatalogoService } from '../catalogos/catalogo.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-pago',
  templateUrl: './pago.page.html',
  styleUrls: ['./pago.page.scss'],
})
export class PagoPage implements OnInit {

  value: String = ''
  users: any[] = [
    {
      id: 1,
      first: '',
      last:'Criptomoneda'
    },
    {
      id: 2,
      first: '',
      last:'Moneda Nacional'
    }, 
  ];
  users1: any[] = [
    {
      id1: 1,
      first1: '',
      last1:'1'
    },
    {
      id1: 2,
      first1: '',
      last1:'5'
    }, 
    {
      id1: 3,
      first1: '',
      last1:'10'
    }, 
  ];
  getSelected: String;

  public productoSeleccionado;
  public cantidad;

  constructor(
    private route: ActivatedRoute,
    public router: Router,
    private catalogoService: CatalogoService) { }

  ngOnInit() {
    this.setProductoSeleccionado();
  }

  setProductoSeleccionado() {
    let id = this.route.snapshot.params.id;
    this.productoSeleccionado = this.catalogoService.find(id);
  }
  
  onChange(selectedValue){
    console.info("Selected:",selectedValue);
  }

  pagoMercantil() {
    let param = JSON.stringify({
      producto:  this.productoSeleccionado,
      cantidad: this.cantidad
    });
    this.router.navigate(['/pago-mercantil', param]);
  }

  pagoBtc() {
    let param = JSON.stringify({
      producto:  this.productoSeleccionado,
      cantidad: this.cantidad
    });
    this.router.navigate(['/criptomoneda', param]);
  }

  pagoBancoVenezuela() {
    let param = JSON.stringify({
      producto:  this.productoSeleccionado,
      cantidad: this.cantidad
    });
    this.router.navigate(['/pago-venezuela', param]);
  }
}
