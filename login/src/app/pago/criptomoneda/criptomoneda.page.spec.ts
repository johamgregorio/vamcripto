import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CriptomonedaPage } from './criptomoneda.page';

describe('CriptomonedaPage', () => {
  let component: CriptomonedaPage;
  let fixture: ComponentFixture<CriptomonedaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CriptomonedaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CriptomonedaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
