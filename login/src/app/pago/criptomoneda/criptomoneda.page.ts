import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DecimalPipe, NumberFormatStyle } from '@angular/common';

@Component({
  selector: 'app-criptomoneda',
  templateUrl: './criptomoneda.page.html',
  styleUrls: ['./criptomoneda.page.scss'],
})
export class CriptomonedaPage implements OnInit {
  
  public producto;
  public tasa = 0.00012;
  public precio;
  public cantidad;
  constructor( private route: ActivatedRoute, public router: Router) { }

  ngOnInit() {
    let id = this.route.snapshot.params.id;
    let detalle = JSON.parse(id);
    
    this.producto = detalle.producto;
    this.cantidad = detalle.cantidad;
    this.precio = (this.producto.precioDolares * this.tasa) * this.cantidad;
  }

  reportarPago() {
    this.router.navigate(['/reporte']);
  }
}
