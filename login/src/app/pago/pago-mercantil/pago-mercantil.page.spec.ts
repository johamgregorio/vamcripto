import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PagoMercantilPage } from './pago-mercantil.page';

describe('PagoMercantilPage', () => {
  let component: PagoMercantilPage;
  let fixture: ComponentFixture<PagoMercantilPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PagoMercantilPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PagoMercantilPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
