import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-pago-mercantil',
  templateUrl: './pago-mercantil.page.html',
  styleUrls: ['./pago-mercantil.page.scss'],
})
export class PagoMercantilPage implements OnInit {

  public producto;
  public tasa = 29000;
  public precio;
  public cantidad;
  constructor( private route: ActivatedRoute, public router: Router) { }

  ngOnInit() {
    let id = this.route.snapshot.params.id;
    let detalle = JSON.parse(id);
    
    this.producto = detalle.producto;
    this.cantidad = detalle.cantidad;
    this.precio = (this.producto.precioDolares * this.tasa) * this.cantidad;
  }

  reportarPago() {
    this.router.navigate(['/reporte']);
  }

}
