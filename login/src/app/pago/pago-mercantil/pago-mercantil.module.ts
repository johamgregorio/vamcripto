import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PagoMercantilPage } from './pago-mercantil.page';

const routes: Routes = [
  {
    path: ':id',
    component: PagoMercantilPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PagoMercantilPage]
})
export class PagoMercantilPageModule {}
