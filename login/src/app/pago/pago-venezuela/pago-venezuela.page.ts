import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-pago-venezuela',
  templateUrl: './pago-venezuela.page.html',
  styleUrls: ['./pago-venezuela.page.scss'],
})
export class PagoVenezuelaPage implements OnInit {

  public producto;
  public tasa = 29000;
  public precio;
  public cantidad;

  constructor(private route: ActivatedRoute, public router: Router) { }

  ngOnInit() {
    let id = this.route.snapshot.params.id;
    let detalle = JSON.parse(id);
    
    this.producto = detalle.producto;
    this.cantidad = detalle.cantidad;
    this.precio = (this.producto.precioDolares * this.tasa) * this.cantidad;
  }

  reportarPago() {
    this.router.navigate(['/reporte']);
  }

}
