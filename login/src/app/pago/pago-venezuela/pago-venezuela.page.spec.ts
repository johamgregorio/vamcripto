import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PagoVenezuelaPage } from './pago-venezuela.page';

describe('PagoVenezuelaPage', () => {
  let component: PagoVenezuelaPage;
  let fixture: ComponentFixture<PagoVenezuelaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PagoVenezuelaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PagoVenezuelaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
