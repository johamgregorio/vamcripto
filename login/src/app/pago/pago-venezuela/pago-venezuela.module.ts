import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PagoVenezuelaPage } from './pago-venezuela.page';

const routes: Routes = [
  {
    path: ':id',
    component: PagoVenezuelaPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PagoVenezuelaPage]
})
export class PagoVenezuelaPageModule {}
