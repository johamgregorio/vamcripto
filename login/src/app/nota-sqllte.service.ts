import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class NotaSQLlteService {

  constructor(private db: AngularFirestore) { }

  getDatosdeConsulta(){
    return this.db.collection('datosConsulta').snapshotChanges()
  }

  addUser(value){
    return new Promise<any>((resolve, reject) => {
      this.db.collection('/datosReporte').add({
        comprobante:value.comprobante,
        telefonoId: value.telefonoId,
        rifId: value.rifId,
        nameId: value.name    
      })
      .then((res) => {
        resolve(res)
      },err => reject(err))
    })
  }

  getEnviar(nombre : string){
    return this.db.collection('/datosReporte').doc(nombre).valueChanges()
  }









}
