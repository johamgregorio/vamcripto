import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.page.html',
  styleUrls: ['./admin.page.scss'],
})
export class AdminPage implements OnInit {

  public correo;
  public nuevoCorreo;
  public password;

  constructor(private authService: AuthService, public router: Router,) { }

  ngOnInit() {

  }

  changeEmail() {
    this.authService
      .updateEmail(this.correo, this.nuevoCorreo, this.password)
      .then(
        this.changeEmailSuccess.bind(this), 
        this.changeEmailError.bind(this)
      );
      this.router.navigate(['/login']);
  }

  changeEmailSuccess(response) {
    console.log('change success', response);
  }

  changeEmailError(error) {
    console.log('change error', error);
  }

}